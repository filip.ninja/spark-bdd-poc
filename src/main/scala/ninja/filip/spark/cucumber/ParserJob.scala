package ninja.filip.spark.cucumber

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import scopt.OptionParser

object ParserJob extends SparkJob {

  case class ParserConfig(inputFile: String)

  override type JobConfig = ParserConfig

  override val initConfig: ParserConfig = ParserConfig("")

  override val sessionBuilder: SparkSession.Builder =
    SparkSession.builder()
      .appName("spark-bdd")
      .master("local[*]")

  override val optionParser: OptionParser[JobConfig] = new scopt.OptionParser[JobConfig]("spark-bdd") {
    head("spark-bdd", "0.0.1")
    opt[String]('i', "inputFile")
      .required()
      .action((x, c) => c.copy(inputFile = x))
      .text("input file name")
    help("help").text("prints this usage text")
  }

  override def run(config: ParserConfig, spark: SparkSession): Unit = {
    spark.read
      .option("header", "true")
      .csv(config.inputFile)
      .withColumnRenamed("A", "foo")
      .withColumnRenamed("B", "bar")
      .withColumnRenamed("C", "foobar")
      .withColumn("foobar",
        when(col("foobar").isNull, lit("none"))
          .otherwise(col("foobar")))
      .write
      .saveAsTable("target")
  }
}
