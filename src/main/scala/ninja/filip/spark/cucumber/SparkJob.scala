package ninja.filip.spark.cucumber

import org.apache.spark.sql.SparkSession
import scopt.OptionParser

trait SparkJob {

  type JobConfig

  val initConfig: JobConfig

  val optionParser: OptionParser[JobConfig]

  val sessionBuilder: SparkSession.Builder

  def run(config: JobConfig, spark: SparkSession)

  def main(args: Array[String]): Unit = {
    print("dupa")

    optionParser.parse(args, initConfig) match {
      case Some(jobconfig) => run(jobconfig, sessionBuilder.getOrCreate())
      case _ => throw new IllegalArgumentException("Failed to parse arguments")
    }
  }
}
