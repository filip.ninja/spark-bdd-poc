package ninja.filip.spark.cucumber

import java.io.File

import io.cucumber.junit.{Cucumber, CucumberOptions}
import org.junit.BeforeClass
import org.junit.runner.RunWith

import scala.reflect.io.Directory

@RunWith(classOf[Cucumber])
@CucumberOptions()
class SparkJobTest

object SparkJobTest {
  @BeforeClass
  def initialize(): Unit = new Directory(new File("./spark-warehouse")).deleteRecursively()
}
