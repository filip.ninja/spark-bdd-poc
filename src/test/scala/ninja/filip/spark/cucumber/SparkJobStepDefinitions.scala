package ninja.filip.spark.cucumber

import cucumber.api.scala.{EN, ScalaDsl}

class SparkJobStepDefinitions extends ScalaDsl with EN {

  val jobUtils = new SparkJobTestUtils()

  Given("Spark job {string}") { (jobClass: String) =>
    jobUtils.withJobClass(jobClass)
  }

  And("""job argument {string}={string}""") { (name: String, value: String) =>
    jobUtils.withArgument(name, value)
  }

  And("csv {string} as table {string}") { (csv: String, table: String) =>
    jobUtils.withTable(csv, table)
  }

  When("job run") {
    jobUtils.run()
  }

  Then("table {string} contains {int} record") { (table: String, count: Int) =>
    jobUtils.assertTableSize(table, count)
  }

  And("Field {string}.{string} is mapped to {string}.{string}") {
    (sourceTable: String, sourceField: String, targetTable: String, targetField: String) =>
      jobUtils.assertColumnsValues(sourceTable, sourceField, targetTable, targetField)
  }

  And("Field {string}.{string} is mapped to {string}.{string} with BusinessRule1") {
    (sourceTable: String, sourceField: String, targetTable: String, targetField: String) =>
      jobUtils.assertFieldValue(targetTable, targetField, "none")
  }
}
