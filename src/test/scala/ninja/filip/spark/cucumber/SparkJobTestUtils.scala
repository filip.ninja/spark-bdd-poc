package ninja.filip.spark.cucumber

import org.apache.spark.sql.SparkSession
import org.junit.Assert._


class SparkJobTestUtils {
  private var job = TestSparkJob()

  System.setProperty("hadoop.home.dir", "C:\\Users\\filip\\Workspace\\winutils\\")

  def withJobClass(jobClass: String): Unit =
    job = job.copy(name = jobClass)

  def withArgument(name: String, value: String): Unit =
    job = job.copy(arguments = job.arguments ++ Seq(s"--${name}=${value}"))

  def withTable(csv: String, name: String): Unit = {
    val sparkJob = getSparkJob(job.name)
    val spark = getSparkSession(sparkJob)
    spark.read.option("header", "true").csv(csv).createOrReplaceTempView(name)
  }

  def run(): Unit = {
    val sparkJob = getSparkJob(job.name)
    sparkJob.main(job.arguments.toArray)
  }

  def assertTableSize(table: String, expectedSize: Int) = {
    val spark = getSparkSession(getSparkJob(job.name))
    val actualSize = spark.table(table).count()
    assertEquals(expectedSize, actualSize)
  }

  def assertColumnsValues(sourceTable: String, sourceColumn: String, targetTable: String, targetColumn: String) = {
    val spark = getSparkSession(getSparkJob(job.name))
    val sourceRow = spark.table(sourceTable).collect().head
    val targetRow = spark.table(targetTable).collect().head
    assertEquals(sourceRow.get(sourceRow.fieldIndex(sourceColumn)), targetRow.get(targetRow.fieldIndex(targetColumn)))
  }

  def assertFieldValue(targetTable: String, targetcolumn: String, value: AnyRef) = {
    val spark = getSparkSession(getSparkJob(job.name))
    val targetRow = spark.table(targetTable).collect().head
    assertEquals(value, targetRow.get(targetRow.fieldIndex(targetcolumn)))
  }

  private def getSparkJob(jobClass: String): SparkJob = {
    val ru = scala.reflect.runtime.universe
    val mirror = ru.runtimeMirror(getClass.getClassLoader);
    val module = mirror.staticModule(jobClass)
    val obj = mirror.reflectModule(module)
    val instanceMirror = mirror.reflect(obj.instance)
    instanceMirror.instance.asInstanceOf[SparkJob]
  }

  private def getSparkSession(sparkJob: SparkJob): SparkSession = sparkJob.sessionBuilder.getOrCreate()
}

case class TestSparkJob(name: String = "",
                        arguments: Seq[String] = Seq.empty)
