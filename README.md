### PoC - Apache Spark with Cucumber

Proof of concept integrating Apache Spark with Cucumber test framework.
It allows defining job specification using Gherkin syntax.

```
Feature: Basic CSV Parser

  Scenario: Parsing correct file with one row
    Given Spark job 'ninja.filip.spark.cucumber.ParserJob'
    * job argument 'inputFile'='src/test/resources/input.csv'
    * csv 'src/test/resources/input.csv' as table 'source'
    When job run
    Then table 'target' contains 1 record
    * Field 'source'.'A' is mapped to 'target'.'foo'
    * Field 'source'.'B' is mapped to 'target'.'bar'
    * Field 'source'.'C' is mapped to 'target'.'foobar' with BusinessRule1
```